package main;

import java.util.HashSet;
import java.util.Set;

import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

public class EugeSpeechletRequestStreamHandler extends SpeechletRequestStreamHandler {
	private static final Set<String> supportedApplicationIds = new HashSet<String>();

	static {
		supportedApplicationIds.add("amzn1.ask.skill.c3157d40-b483-489d-9ff8-af5b010240a6");

	}

	public EugeSpeechletRequestStreamHandler() {
		super(new EugeSpeechlet(), supportedApplicationIds);
	}
}
